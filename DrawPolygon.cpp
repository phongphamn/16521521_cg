#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{

}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], xp[8];
	int y[8], yp[8];
	float phi = 0;
	float PI = 3.14;
	float r = R * sin(PI / 8) / sin(6 * PI / 8);
	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xp[i] = xc + int(r*cos(phi + PI / 8) + 0.5);
		yp[i] = yc - int(r*sin(phi + PI / 8) + 0.5);
		phi += 2 * PI / 8;
	}
	for (int i = 0; i < 8; i++)
	{
		SDL_RenderDrawPoint(ren, x[i], y[i]);
	}
	for (int i = 0; i < 8; i++)
	{
		DDA_Line(x[i], y[i], xp[i], yp[i], ren);
		DDA_Line(xp[i], yp[i], x[(i + 1) % 8], y[(i + 1) % 8], ren);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{

}
