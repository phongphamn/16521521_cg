#include "Ellipse.h"
#include <math.h>

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;
	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc - x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc - x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc + x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int a2 = a * a;
	int b2 = b * b;
	int fa2 = 4 * a2, fb2 = 4 * b2;
	int x , y, p;


	//ve nhanh thu 1(tu tren xuong )
	for (x = 0, y = b, p = 2 * b2 + a2*(1 - 2 * b); b2 * x <= a2 * y; x++)
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p >= 0)
		{
			p += fa2 *(1 - y);
			y--;
		}
		p += b2 *((4 * x) + 6);
	}
	// Area 2
	for (x = a, y = 0, p = 2 * a2 + b2*(1 - 2 * a); a2*y <= b2*x; y++)
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p >= 0)
		{
			p += fb2 * (1 - x);
			x--;
		}
		p += a2 * ((4 * y) + 6);
	}
	
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int a2 = a* a;
	int b2 = b*b;
	int x = 0;
	int y = b;
	int p;

	for (x = 0, y = b, p = b2 - a2*x + .25*y; 2 * b2 * x <= 2 * a2* y; x++)
	{
		if (p < 0)
		{
			p += 2 * b2 * x + b2;
		}
		else
		{
			y--;
			p = p + 2 * b2*x - 2 * a2*y - b2;
		}
		Draw4Points(xc, yc, x, y, ren);
	}


	
	for (p = b2*(x + 0.5)*(x + 0.5) + a2*(y - 1)*(y - 1) - a2*b2; y > 0; y--)
	{
		
		if (p <= 0)
		{
			x++;
			p = p + 2 * b2*x - 2 * a2*y + a2;
		}
		else
		{
			p = p - 2 * a2*y + a2;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
}
