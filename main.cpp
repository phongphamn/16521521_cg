#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "FillColor.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**){
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL){
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL){
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}
	SDL_Color fillColor;
	fillColor.r = 180;
	fillColor.b = 180;
	fillColor.a = 0;
	fillColor.g = 0;
	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);
	
	//YOU CAN INSERT CODE FOR TESTING HERE
	Vector2D v1(200, 200), v2(300,300), v3(350,350);
	BresenhamDrawCircle(200, 200, 150, ren);

	//TriangleFill(v1, v2, v3, ren, fillColor);
	//FillIntersectionEllipseCircle(200, 250, 50, 70, 250, 300, 70, ren, fillColor);
	//FillIntersectionTwoCircles(200,250, 50, 250, 300, 70, ren, fillColor);
	//RectangleFill(v1, v3, ren, fillColor);
	//CircleFill(200, 300, 50, ren, fillColor);
	//DrawCurve2(ren, v1, v2, v3);

	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event

	bool running = true;

	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{

			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
		}

	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
