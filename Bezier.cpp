#include "Bezier.h"
#include <iostream>
using namespace std;


void DrawCircle(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc + y, yc + x);
	SDL_RenderDrawPoint(ren, xc - y, yc + x);
	SDL_RenderDrawPoint(ren, xc + y, yc - x);
	SDL_RenderDrawPoint(ren, xc - y, yc - x);
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0, y = R;
	int d = 3 - 2 * R;
	while (y >= x)
	{
		DrawCircle(xc, yc, x, y, ren);
		x++;
		if (d > 0)
		{
			y--;
			d = d + 4 * (x - y) + 10;
		}
		else
			d = d + 4 * x + 6;
		DrawCircle(xc, yc, x, y, ren);
	}
}
void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	double t, a, b, c, xb, yb;
	for (t = 0.0; t < 1.0; t += 0.00005)
	{
		a = (1 - t)*(1 - t);
		b = 2 * t*(1 - t);
		c = t*t;
		xb = a*p1.x + b*p2.x + c*p3.x;
		yb = a*p1.y + b*p2.y + c*p3.y;
		SDL_RenderDrawPoint(ren, xb, yb);
	}
	BresenhamDrawCircle(p1.x, p1.y, 50, ren);
	BresenhamDrawCircle(p2.x, p2.y, 50, ren);
	BresenhamDrawCircle(p3.x, p3.y, 50, ren);
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{

}
