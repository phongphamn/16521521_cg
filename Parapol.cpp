#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;
	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc + x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int a, SDL_Renderer *ren)
{

	double x = 0, y = 0;  /* initial coorodinates */

	double d1;

	d1 = 2 * a - 1;

	Draw2Points(xc, yc, x, y, ren);

	while (y <= (2 * a*1.0))

	{

		if (d1 < 0)

		{

			d1 += 4 * a - 3 - 2 * y;

			x++;

			y++;

		}

		else

		{

			d1 -= 3 + 2 * y;

			y++;

		}

		Draw2Points(xc, yc, x, y, ren);

	}

	d1 = (4.0*a*(x + 1) - (y + 0.5)*(y + 0.5));

	while (y < 220)

	{

		if (d1 < 0)

		{

			d1 += 4 * a;

			x++;

		}

		else

		{

			d1 += 4.0*a - 2 - 2.0*y;

			x++;

			y++;

		}

		Draw2Points(xc, yc, x, y, ren);
	}

}


void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
}
