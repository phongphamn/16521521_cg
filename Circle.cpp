﻿#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;
	//1
	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);//vẽ một điểm
	//2
	new_x = xc + y;
	new_y = yc + x;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	//3
	new_x = xc + y;
	new_y = yc - x;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	//4
	new_x = xc + x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	//5
	new_x = xc - x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	//6
	new_x = xc - y;
	new_y = yc - x;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	//7
	new_x = xc - y;
	new_y = yc + x;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	//8
	new_x = xc - x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	
}

//Tâm (xc, yc) - bán kính R
void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int p = 3 - 2 * R;
	int x = 0, y = R;
	while (x <= y)
	{
		Draw8Points(xc, yc, x, y, ren);
		if (p < 0)
		{
			p = p +  4 * x + 6;
		}
		else
		{
			
			p = p + 4 * (x - y) + 10;
			y--;
		}
		x++;
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x, y, p;
	x = 0;
	y = R;
	Draw8Points(xc, yc, x, y, ren);
	p = 5 / 4 - R;
	while (x <= y)
	{
		if (p < 0)
		{
			p = p + 2 * x + 3;
		}
		else
		{
			y = y - 1;
			p = p + 2 * (x - y) + 5;
			
		}
		x = x + 1;
		Draw8Points(xc, yc, x, y, ren);
	}
}
