#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
	RECT rect;
	rect.Left = l;
	rect.Right = r;
	rect.Top = t;
	rect.Bottom = b;

	return rect;
}

CODE Encode(RECT r, Vector2D P)
{
	CODE c = 0;
	if (P.x < r.Left)
		c = c | LEFT;
	if (P.x > r.Right)
		c = c | RIGHT;
	if (P.y < r.Top)
		c = c | TOP;
	if (P.y > r.Bottom)
		c = c | BOTTOM;
	return c;
}

int CheckCase(int c1, int c2)
{
	if (c1 == 0 && c2 == 0)
		return 1;
	if (c1 != 0 && c2 != 0 && c1&c2 != 0)
		return 2;
	return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	int code1 = Encode(r, P1);
	int code2 = Encode(r, P2);

	bool accept = false;

	int ck = CheckCase(code1, code1);
	while (ck == 3)
	{
		ClippingCohenSutherland(r, P1, P2);
		code1 = Encode(r, P1);
		code2 = Encode(r, P2);
		ck = CheckCase(code1, code2);
	}
	if (ck == 2)
		return 0;
	Q1 = P1;
	Q2 = P2;
	return 1;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	int code1 = Encode(r, P1);
	int code2 = Encode(r, P2);
	int code_out;
	Vector2D P;
	if (code1 != 0)
		code_out = code1;
	else
		code_out = code2;
	if (code_out & BOTTOM)
	{
		P.x = P1.x + (P2.x - P1.x) * (r.Bottom - P1.y) / (P2.y - P1.y);
		P.y = r.Bottom;
		return;
	}
	else if (code_out & TOP)
	{
		P.x = P1.x + (P2.x - P1.x) * (r.Top - P1.y) / (P2.y = P1.y);
		P.y = r.Top;
		return;
	}
	else if (code_out & RIGHT)
	{
		P.y = P1.y + (P2.y - P1.y) * (r.Right - P1.x) / (P2.x - P1.x);
		P.x = r.Right;
		return;
	}
	else if (code_out & LEFT)
	{
		P.y = P1.y + (P2.y - P1.y) * (r.Left - P1.x) / (P2.x - P1.x);
		P.x = r.Left;
		return;
	}
}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
	if (p == 0)
	{
		if (q < 0)
			return 0;
		return 1;
	}

	if (p > 0)
	{
		float t = (float)q / p;
		if (t2<t)
			return 1;
		if (t<t1)
			return 0;
		t2 = t;
		return 1;
	}

	float t = (float)q / p;
	if (t2<t)
		return 0;
	if (t<t1)
		return 1;
	t1 = t;
	return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	int p[4], q[4], i, accept = 1;
	float u[4], umin = 0, umax = 1;
	p[0] = -(P2.x - P1.x);
	p[1] = (P2.x - P1.x);
	p[2] = (P2.y - P1.y);
	p[3] = -(P2.y - P1.y);
	
	q[0] = P1.x - r.Left;
	q[1] = r.Right - P1.x;
	q[2] = P1.y - r.Bottom;
	q[3] = r.Top - P1.y;

	for (i = 0; i < 4; i++)
	{
		if (p[i] == 0)
		{
			if (q[i] >= 0)
			{
				if (i < 2)
				{
					if (P1.y < r.Bottom)
					{
						P1.y = r.Bottom;
					}
					if (P2.y > r.Top)
					{
						P2.y = r.Top;
					}
				}
			}
			if (i > 1)
			{
				if (P1.x < r.Left)
				{
					P1.x = r.Left;
				}
				if (P2.x > r.Right)
				{
					P2.x = r.Right;
				}
			}
		}
	}
	float t1, t2, temp;
	t1 = 0;
	t2 = 0;
	for (i = 0; i < 4; i++)
	{
		SolveNonLinearEquation(p[i], q[i], t1, t2);
	}
	
	if (t1 < t2)
	{
		Q1.x = P1.x + t1 * p[1];
		Q2.x = P1.x + t2 * p[1];
		Q1.y = P1.y + t1 * p[3];
		Q2.y = P1.y + t2 * p[3];
	}
	return 1;
}
